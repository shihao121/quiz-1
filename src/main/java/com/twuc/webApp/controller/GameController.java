package com.twuc.webApp.controller;

import com.twuc.webApp.dto.RequestDto;
import com.twuc.webApp.dto.ResponseDto;
import com.twuc.webApp.dto.ResponseGuessDto;
import com.twuc.webApp.service.GameService;
import com.twuc.webApp.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/games")
@CrossOrigin
public class GameController {

    @Autowired
    private GameService gameService;

    @PostMapping()
    public ResponseEntity createGames(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        String locationHeader = String.format("%s/%d",requestURI, ++Constant.gameId);
        gameService.createGame(Constant.gameId);
        return ResponseEntity.status(HttpStatus.CREATED).header("Location", locationHeader).
                header("Access-Control-Expose-Headers", "Location").build();
    }

    @GetMapping(value = "/{gameId}")
    public ResponseDto getGameStatus(@PathVariable Long gameId) {
        return gameService.getGameStatus(gameId);
    }

    @PatchMapping(value = "/{gameId}")
    public ResponseGuessDto judgeGuess(@PathVariable Long gameId,
                                       @RequestBody @Valid RequestDto requestDto) {
        return gameService.judgeGuess(requestDto.getAnswer(), getGameStatus(gameId).getAnswer());
    }
}
