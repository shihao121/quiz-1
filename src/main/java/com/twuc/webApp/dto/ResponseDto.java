package com.twuc.webApp.dto;

public class ResponseDto {
    private Long gameId;
    private Integer answer;

    public ResponseDto(Long gameId, Integer answer) {
        this.gameId = gameId;
        this.answer = answer;
    }

    public ResponseDto() {
    }

    public Long getGameId() {
        return gameId;
    }

    public Integer getAnswer() {
        return answer;
    }
}
