package com.twuc.webApp.dto;

public class ResponseGuessDto {

    private String hint;
    private Boolean correct;

    public ResponseGuessDto(String hint, Boolean correct) {
        this.hint = hint;
        this.correct = correct;
    }

    public ResponseGuessDto() {
    }

    public String getHint() {
        return hint;
    }

    public Boolean getCorrect() {
        return correct;
    }
}

