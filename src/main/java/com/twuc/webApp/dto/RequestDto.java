package com.twuc.webApp.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class RequestDto {

    @Min(1111) @Max(9999)
    private Integer answer;

    public RequestDto(Integer answer) {
        this.answer = answer;
    }

    public RequestDto() {
    }

    public Integer getAnswer() {
        return answer;
    }
}
