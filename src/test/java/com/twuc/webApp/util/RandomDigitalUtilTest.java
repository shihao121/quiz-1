package com.twuc.webApp.util;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class RandomDigitalUtilTest {

    @Test
    void should_return_4_distinct_digital() {
        Set<Integer> randomSet = DigitalUtil.generate4RandomDigital();
        assertEquals(randomSet.size(), 4);
        assertEquals(4, randomSet.stream().filter(number -> {
            return number < 10 && number >= 0;
        }).count());
    }

    @Test
    void should_get_equal_count() {
        Long equalCount = DigitalUtil.getEqualCount(23456, 56478, 5);
        Long anotherEqualCount = DigitalUtil.getEqualCount(3456, 3476, 4);
        assertEquals(1L, (long) equalCount);
        assertEquals(3L, (long) anotherEqualCount);
    }

    @Test
    void should_get_contains_count() {
        Long containsCount = DigitalUtil.getContainsCount(2345, 2354, 4);
        Long anotherContainsCount = DigitalUtil.getContainsCount(78965, 46793, 5);
        assertEquals(2L, (long) containsCount);
        assertEquals(3L, (long) anotherContainsCount);
    }
}