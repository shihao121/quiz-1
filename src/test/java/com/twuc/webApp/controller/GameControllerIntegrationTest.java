package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_201_code_when_create_games() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games"))
                .andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    void should_return_location_header_when_create_games() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.post("/api/games")))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().string("Location", "/api/games/1"));
        mockMvc.perform((MockMvcRequestBuilders.post("/api/games")))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().string("Location", "/api/games/2"));
    }

    @Test
    void should_return_200_with_game_status_when_request_game_url() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.post("/api/games")))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().exists("Location"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/api/games/1"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.gameId").value(1))
                .andExpect(jsonPath("$.answer").exists())
                .andReturn();
        System.out.println(mvcResult.getResponse().getContentAsString());
    }

    @Test
    void should_return_200_with_message_when_patch_guess_request() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.post("/api/games")))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().exists("Location"));
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{ \"answer\": \"1234\" }"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.hint").exists())
                .andExpect(jsonPath("$.correct").isBoolean());
    }

    @Test
    void should_return_400_when_gameId_not_number() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/a"))
                .andExpect(MockMvcResultMatchers.status().is(400));

    }

    @Test
    void should_return_400_when_game_id_not_exist() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/19"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_400_when_answer_beyond_four_digital() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.post("/api/games")))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().exists("Location"));
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{ \"answer\": \"12345\" }"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_400_when_answer_not_number() throws Exception {
        mockMvc.perform((MockMvcRequestBuilders.post("/api/games")))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().exists("Location"));
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{ \"answer\": \"hell\" }"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
}